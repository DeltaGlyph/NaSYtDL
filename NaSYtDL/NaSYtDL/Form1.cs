﻿/*
Written 6/30/2016 by DeltaGlyph
This program will download and convert YouTube videos into playable audio files
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoLibrary;
using MediaToolkit.Model;
using System.Net;
using System.IO;
using MediaToolkit;

namespace NaSYtDL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cbAudioFormat.DataSource = Enum.GetValues(typeof(AudioFormat));
            btnRun.Enabled = false;
        }
        //Set fields for use
        string videoID;
        string downloadLocation;
        string lastWrittenVideo;
        YouTube yt = YouTube.Default;
        YouTubeVideo video;
        private enum AudioFormat
        {
            MP3,
            FLAC
        };
        
        private void btnDLSelect_Click(object sender, EventArgs e)
        {
            DialogResult result = diagSelectDLFolder.ShowDialog();
            if (result == DialogResult.OK)
            {
                downloadLocation = diagSelectDLFolder.SelectedPath+"\\";
                btnRun.Enabled = true;
                lblDownloadLocation.Text = downloadLocation;
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            videoID = "";
            //Check that videoID has been properly assigned
            if (tabControl.SelectedTab == tabControl.TabPages["tabSingle"])
            {
                setVideoID();
                if (!String.Equals(videoID, ""))
                {
                    //Run the video download
                    downloadVideo(videoID);
                    convertVideo();
                    deleteVideo();
                    MessageBox.Show("Conversion completed", "INFO");

                }
            }
            if (tabControl.SelectedTab == tabControl.TabPages["tabQueue"])
            {
                foreach (string video in lstVideoQueue.Items)
                {
                    downloadVideo(video);
                    convertVideo();
                    deleteVideo();
                }
                lstVideoQueue.Items.Clear();
                MessageBox.Show("Queue completed", "INFO");

            }
        }
        //Validate YouTube links by making sure the page exists before downloading
        private bool validateURL(string URL)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(URL) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse reponse = request.GetResponse() as HttpWebResponse;
                reponse.Close();
                return (reponse.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                MessageBox.Show("Invalid URL entered", "ERROR");
                return false;
            }
        }
        //Set videoID while validating URL
        private void setVideoID()
        {
            string ID = txtURL.Text;
            bool valid = validateURL(ID);
            if (valid == true)
            {
                videoID = ID;
            }
        }
        //Download video inserted as string link
        private void downloadVideo(string link)
        {
            video = yt.GetVideo(link);
            File.WriteAllBytes(downloadLocation + video.FullName, video.GetBytes());
            lastWrittenVideo = downloadLocation + video.FullName;
        }
        //Set audio format for converted video
        private string getAudioFormat()
        {
            string aFormat;
            aFormat = "."+cbAudioFormat.SelectedValue.ToString().ToLower();
            return aFormat;
        }
        //run conversion on downloaded video
        private void convertVideo()
        {
            var inputFile = new MediaFile { Filename = lastWrittenVideo};
            var outputFile = new MediaFile { Filename = getOutputFileName()};
            using (var engine = new Engine())
            {
                engine.Convert(inputFile, outputFile);
            }
        }
        //Create string with correct output file name
        private string getOutputFileName()
        {
            string filename;
            filename = string.Format(@"{0}{1}", downloadLocation,video.FullName);
            filename = filename.Replace(video.FileExtension, getAudioFormat());
            return filename;
        }
        //Delete video after conversion finishes
        private void deleteVideo()
        {
            File.Delete(string.Format(@"{0}{1}",downloadLocation,video.FullName));
        }

        private void btnAddQ_Click(object sender, EventArgs e)
        {
            bool validFlag = validateURL(txtVQEntry.Text);
            if (validFlag == true)
            {
                lstVideoQueue.Items.Add(txtVQEntry.Text);
                txtVQEntry.Clear();
            }
            else
            {
                MessageBox.Show("Invalid URL", "ERROR");
            }
        }

        private void btnDelVideo_Click(object sender, EventArgs e)
        {
            lstVideoQueue.Items.RemoveAt(lstVideoQueue.SelectedIndex);
        }
    }
}
