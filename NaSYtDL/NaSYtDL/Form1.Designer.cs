﻿namespace NaSYtDL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRun = new System.Windows.Forms.Button();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.diagSelectDLFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.btnDLSelect = new System.Windows.Forms.Button();
            this.cbAudioFormat = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDownloadLocation = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabSingle = new System.Windows.Forms.TabPage();
            this.tabQueue = new System.Windows.Forms.TabPage();
            this.lstVideoQueue = new System.Windows.Forms.ListBox();
            this.btnAddQ = new System.Windows.Forms.Button();
            this.txtVQEntry = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDelVideo = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.tabSingle.SuspendLayout();
            this.tabQueue.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(135, 182);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(105, 40);
            this.btnRun.TabIndex = 0;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(8, 25);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(344, 20);
            this.txtURL.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter YouTube URL here";
            // 
            // diagSelectDLFolder
            // 
            this.diagSelectDLFolder.RootFolder = System.Environment.SpecialFolder.UserProfile;
            // 
            // btnDLSelect
            // 
            this.btnDLSelect.Location = new System.Drawing.Point(12, 182);
            this.btnDLSelect.Name = "btnDLSelect";
            this.btnDLSelect.Size = new System.Drawing.Size(117, 40);
            this.btnDLSelect.TabIndex = 3;
            this.btnDLSelect.Text = "Select Download Folder";
            this.btnDLSelect.UseVisualStyleBackColor = true;
            this.btnDLSelect.Click += new System.EventHandler(this.btnDLSelect_Click);
            // 
            // cbAudioFormat
            // 
            this.cbAudioFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAudioFormat.FormattingEnabled = true;
            this.cbAudioFormat.Location = new System.Drawing.Point(77, 57);
            this.cbAudioFormat.Name = "cbAudioFormat";
            this.cbAudioFormat.Size = new System.Drawing.Size(121, 21);
            this.cbAudioFormat.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Convert To:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Download Location:";
            // 
            // lblDownloadLocation
            // 
            this.lblDownloadLocation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDownloadLocation.Location = new System.Drawing.Point(113, 89);
            this.lblDownloadLocation.Name = "lblDownloadLocation";
            this.lblDownloadLocation.Size = new System.Drawing.Size(229, 23);
            this.lblDownloadLocation.TabIndex = 7;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabSingle);
            this.tabControl.Controls.Add(this.tabQueue);
            this.tabControl.Location = new System.Drawing.Point(12, 15);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(491, 159);
            this.tabControl.TabIndex = 8;
            // 
            // tabSingle
            // 
            this.tabSingle.Controls.Add(this.cbAudioFormat);
            this.tabSingle.Controls.Add(this.lblDownloadLocation);
            this.tabSingle.Controls.Add(this.label3);
            this.tabSingle.Controls.Add(this.txtURL);
            this.tabSingle.Controls.Add(this.label2);
            this.tabSingle.Controls.Add(this.label1);
            this.tabSingle.Location = new System.Drawing.Point(4, 22);
            this.tabSingle.Name = "tabSingle";
            this.tabSingle.Padding = new System.Windows.Forms.Padding(3);
            this.tabSingle.Size = new System.Drawing.Size(483, 133);
            this.tabSingle.TabIndex = 0;
            this.tabSingle.Text = "Single Video";
            this.tabSingle.UseVisualStyleBackColor = true;
            // 
            // tabQueue
            // 
            this.tabQueue.Controls.Add(this.btnDelVideo);
            this.tabQueue.Controls.Add(this.label4);
            this.tabQueue.Controls.Add(this.txtVQEntry);
            this.tabQueue.Controls.Add(this.btnAddQ);
            this.tabQueue.Controls.Add(this.lstVideoQueue);
            this.tabQueue.Location = new System.Drawing.Point(4, 22);
            this.tabQueue.Name = "tabQueue";
            this.tabQueue.Padding = new System.Windows.Forms.Padding(3);
            this.tabQueue.Size = new System.Drawing.Size(483, 133);
            this.tabQueue.TabIndex = 1;
            this.tabQueue.Text = "Video Queue";
            this.tabQueue.UseVisualStyleBackColor = true;
            // 
            // lstVideoQueue
            // 
            this.lstVideoQueue.FormattingEnabled = true;
            this.lstVideoQueue.Location = new System.Drawing.Point(6, 6);
            this.lstVideoQueue.Name = "lstVideoQueue";
            this.lstVideoQueue.Size = new System.Drawing.Size(176, 121);
            this.lstVideoQueue.TabIndex = 0;
            // 
            // btnAddQ
            // 
            this.btnAddQ.Location = new System.Drawing.Point(188, 73);
            this.btnAddQ.Name = "btnAddQ";
            this.btnAddQ.Size = new System.Drawing.Size(124, 23);
            this.btnAddQ.TabIndex = 1;
            this.btnAddQ.Text = "Add Video to Queue";
            this.btnAddQ.UseVisualStyleBackColor = true;
            this.btnAddQ.Click += new System.EventHandler(this.btnAddQ_Click);
            // 
            // txtVQEntry
            // 
            this.txtVQEntry.Location = new System.Drawing.Point(189, 47);
            this.txtVQEntry.Name = "txtVQEntry";
            this.txtVQEntry.Size = new System.Drawing.Size(288, 20);
            this.txtVQEntry.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(188, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Enter YouTube Link Below";
            // 
            // btnDelVideo
            // 
            this.btnDelVideo.Location = new System.Drawing.Point(189, 103);
            this.btnDelVideo.Name = "btnDelVideo";
            this.btnDelVideo.Size = new System.Drawing.Size(123, 23);
            this.btnDelVideo.TabIndex = 4;
            this.btnDelVideo.Text = "Delete Video";
            this.btnDelVideo.UseVisualStyleBackColor = true;
            this.btnDelVideo.Click += new System.EventHandler(this.btnDelVideo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 234);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnDLSelect);
            this.Controls.Add(this.btnRun);
            this.Name = "Form1";
            this.Text = "Not A Shitty YouTube Downloader";
            this.tabControl.ResumeLayout(false);
            this.tabSingle.ResumeLayout(false);
            this.tabSingle.PerformLayout();
            this.tabQueue.ResumeLayout(false);
            this.tabQueue.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog diagSelectDLFolder;
        private System.Windows.Forms.Button btnDLSelect;
        private System.Windows.Forms.ComboBox cbAudioFormat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDownloadLocation;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabSingle;
        private System.Windows.Forms.TabPage tabQueue;
        private System.Windows.Forms.Button btnAddQ;
        private System.Windows.Forms.ListBox lstVideoQueue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVQEntry;
        private System.Windows.Forms.Button btnDelVideo;
    }
}

