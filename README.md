Makin a youtube downloader and converter that isn't full of garbage


# COMPONENTS

NaSYtDL uses the following components

[Media Toolkit](https://github.com/AydinAdn/MediaToolkit) is licensed under the [MIT License](https://github.com/AydinAdn/MediaToolkit/blob/master/LICENSE.md)

[Libvideo](https://github.com/jamesqo/libvideo) is licensed under the [BSD 2-Clause License](https://github.com/jamesqo/libvideo/blob/master/bsd.license)

# LICENSE

Not A Shitty YouTube Downloader is released under the [BSD 3-Clause License](https://gitlab.com/DeltaGlyph/NaSYtDL/blob/master/LICENSE)